# Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fee** | **str** |  | [optional] 
**stake_units** | **str** |  | [optional] 
**slip** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

