# TxDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pool** | [**Asset**](Asset.md) |  | [optional] 
**type** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**_in** | [**Tx**](Tx.md) |  | [optional] 
**out** | [**list[Tx]**](Tx.md) |  | [optional] 
**_date** | **int** |  | [optional] 
**gas** | [**Gas**](Gas.md) |  | [optional] 
**options** | [**Option**](Option.md) |  | [optional] 
**height** | **str** |  | [optional] 
**events** | [**Event**](Event.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

