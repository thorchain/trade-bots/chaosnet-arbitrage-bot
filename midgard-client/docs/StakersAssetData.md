# StakersAssetData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset** | [**Asset**](Asset.md) |  | [optional] 
**stake_units** | **str** | Represents ownership of a pool. | [optional] 
**date_first_staked** | **int** |  | [optional] 
**height_last_staked** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

