# swagger_client.DefaultApi

All URIs are relative to *http://18.159.173.48:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_asset_info**](DefaultApi.md#get_asset_info) | **GET** /v1/assets | Get Asset Information
[**get_health**](DefaultApi.md#get_health) | **GET** /v1/health | Get Health
[**get_network_data**](DefaultApi.md#get_network_data) | **GET** /v1/network | Get Network Data
[**get_nodes**](DefaultApi.md#get_nodes) | **GET** /v1/nodes | Get Node public keys
[**get_pools**](DefaultApi.md#get_pools) | **GET** /v1/pools | Get Asset Pools
[**get_pools_details**](DefaultApi.md#get_pools_details) | **GET** /v1/pools/detail | Get Pools Details
[**get_stakers_address_and_asset_data**](DefaultApi.md#get_stakers_address_and_asset_data) | **GET** /v1/stakers/{address}/pools | Get Staker Pool Data
[**get_stakers_address_data**](DefaultApi.md#get_stakers_address_data) | **GET** /v1/stakers/{address} | Get Staker Data
[**get_stakers_data**](DefaultApi.md#get_stakers_data) | **GET** /v1/stakers | Get Stakers
[**get_stats**](DefaultApi.md#get_stats) | **GET** /v1/stats | Get Global Stats
[**get_thorchain_proxied_constants**](DefaultApi.md#get_thorchain_proxied_constants) | **GET** /v1/thorchain/constants | Get the Proxied THORChain Constants
[**get_thorchain_proxied_endpoints**](DefaultApi.md#get_thorchain_proxied_endpoints) | **GET** /v1/thorchain/pool_addresses | Get the Proxied Pool Addresses
[**get_thorchain_proxied_lastblock**](DefaultApi.md#get_thorchain_proxied_lastblock) | **GET** /v1/thorchain/lastblock | Get the Proxied THORChain Lastblock
[**get_total_vol_changes**](DefaultApi.md#get_total_vol_changes) | **GET** /v1/history/total_volume | Get Total Volume Changes
[**get_tx_details**](DefaultApi.md#get_tx_details) | **GET** /v1/txs | Get details of a tx by address, asset or tx-id

# **get_asset_info**
> list[AssetDetail] get_asset_info(asset)

Get Asset Information

Detailed information about a specific asset. Returns enough information to display a unique asset in various user interfaces, including latest price.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
asset = 'asset_example' # str | One or more comma separated unique asset (CHAIN.SYMBOL)

try:
    # Get Asset Information
    api_response = api_instance.get_asset_info(asset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_asset_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset** | **str**| One or more comma separated unique asset (CHAIN.SYMBOL) | 

### Return type

[**list[AssetDetail]**](AssetDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_health**
> InlineResponse200 get_health()

Get Health

Returns an object containing the health response of the API.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get Health
    api_response = api_instance.get_health()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_health: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_network_data**
> NetworkInfo get_network_data()

Get Network Data

Returns an object containing Network data

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get Network Data
    api_response = api_instance.get_network_data()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_network_data: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NetworkInfo**](NetworkInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_nodes**
> list[NodeKey] get_nodes()

Get Node public keys

Returns an object containing Node public keys

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get Node public keys
    api_response = api_instance.get_nodes()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_nodes: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[NodeKey]**](NodeKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pools**
> list[Asset] get_pools()

Get Asset Pools

Returns an array containing all the assets supported on BEPSwap pools

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get Asset Pools
    api_response = api_instance.get_pools()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_pools: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Asset]**](Asset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pools_details**
> list[PoolDetail] get_pools_details(asset, view=view)

Get Pools Details

Returns an object containing all the pool details for that asset.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
asset = 'asset_example' # str | One or more comma separated unique asset (CHAIN.SYMBOL)
view = 'full' # str | Specifies the returning view (optional) (default to full)

try:
    # Get Pools Details
    api_response = api_instance.get_pools_details(asset, view=view)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_pools_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **asset** | **str**| One or more comma separated unique asset (CHAIN.SYMBOL) | 
 **view** | **str**| Specifies the returning view | [optional] [default to full]

### Return type

[**list[PoolDetail]**](PoolDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_stakers_address_and_asset_data**
> list[StakersAssetData] get_stakers_address_and_asset_data(address, asset)

Get Staker Pool Data

Returns an object containing staking data for the specified staker and pool.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
address = 'address_example' # str | Unique staker address
asset = 'asset_example' # str | One or more comma separated unique asset (CHAIN.SYMBOL)

try:
    # Get Staker Pool Data
    api_response = api_instance.get_stakers_address_and_asset_data(address, asset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_stakers_address_and_asset_data: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **address** | **str**| Unique staker address | 
 **asset** | **str**| One or more comma separated unique asset (CHAIN.SYMBOL) | 

### Return type

[**list[StakersAssetData]**](StakersAssetData.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_stakers_address_data**
> StakersAddressData get_stakers_address_data(address)

Get Staker Data

Returns an array containing all the pools the staker is staking in.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
address = 'address_example' # str | Unique staker address

try:
    # Get Staker Data
    api_response = api_instance.get_stakers_address_data(address)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_stakers_address_data: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **address** | **str**| Unique staker address | 

### Return type

[**StakersAddressData**](StakersAddressData.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_stakers_data**
> list[Stakers] get_stakers_data()

Get Stakers

Returns an array containing the addresses for all stakers.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get Stakers
    api_response = api_instance.get_stakers_data()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_stakers_data: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Stakers]**](Stakers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_stats**
> StatsData get_stats()

Get Global Stats

Returns an object containing global stats for all pools and all transactions.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get Global Stats
    api_response = api_instance.get_stats()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_stats: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatsData**](StatsData.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_thorchain_proxied_constants**
> ThorchainConstants get_thorchain_proxied_constants()

Get the Proxied THORChain Constants

Returns a proxied endpoint for the constants endpoint from a local thornode

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get the Proxied THORChain Constants
    api_response = api_instance.get_thorchain_proxied_constants()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_thorchain_proxied_constants: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ThorchainConstants**](ThorchainConstants.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_thorchain_proxied_endpoints**
> ThorchainEndpoints get_thorchain_proxied_endpoints()

Get the Proxied Pool Addresses

Returns a proxied endpoint for the pool_addresses endpoint from a local thornode

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get the Proxied Pool Addresses
    api_response = api_instance.get_thorchain_proxied_endpoints()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_thorchain_proxied_endpoints: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ThorchainEndpoints**](ThorchainEndpoints.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_thorchain_proxied_lastblock**
> ThorchainLastblock get_thorchain_proxied_lastblock()

Get the Proxied THORChain Lastblock

Returns a proxied endpoint for the lastblock endpoint from a local thornode

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get the Proxied THORChain Lastblock
    api_response = api_instance.get_thorchain_proxied_lastblock()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_thorchain_proxied_lastblock: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ThorchainLastblock**](ThorchainLastblock.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_total_vol_changes**
> list[TotalVolChanges] get_total_vol_changes(interval, _from, to)

Get Total Volume Changes

Returns total volume changes of all pools in specified interval

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
interval = 'interval_example' # str | Interval of calculations
_from = 789 # int | Start time of the query as unix timestamp
to = 789 # int | End time of the query as unix timestamp

try:
    # Get Total Volume Changes
    api_response = api_instance.get_total_vol_changes(interval, _from, to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_total_vol_changes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **interval** | **str**| Interval of calculations | 
 **_from** | **int**| Start time of the query as unix timestamp | 
 **to** | **int**| End time of the query as unix timestamp | 

### Return type

[**list[TotalVolChanges]**](TotalVolChanges.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_tx_details**
> InlineResponse2001 get_tx_details(offset, limit, address=address, txid=txid, asset=asset, type=type)

Get details of a tx by address, asset or tx-id

Return an array containing the event details

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
offset = 789 # int | pagination offset
limit = 789 # int | pagination limit
address = 'address_example' # str | Address of sender or recipient of any in/out tx in event (optional)
txid = 'txid_example' # str | ID of any in/out tx in event (optional)
asset = 'asset_example' # str | Any asset used in event (CHAIN.SYMBOL) (optional)
type = 'type_example' # str | One or more comma separated unique types of event (optional)

try:
    # Get details of a tx by address, asset or tx-id
    api_response = api_instance.get_tx_details(offset, limit, address=address, txid=txid, asset=asset, type=type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_tx_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int**| pagination offset | 
 **limit** | **int**| pagination limit | 
 **address** | **str**| Address of sender or recipient of any in/out tx in event | [optional] 
 **txid** | **str**| ID of any in/out tx in event | [optional] 
 **asset** | **str**| Any asset used in event (CHAIN.SYMBOL) | [optional] 
 **type** | **str**| One or more comma separated unique types of event | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

