# ThorchainConstants

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**int_64_values** | [**ThorchainInt64Constants**](ThorchainInt64Constants.md) |  | [optional] 
**bool_values** | [**ThorchainBooleanConstants**](ThorchainBooleanConstants.md) |  | [optional] 
**string_values** | [**ThorchainStringConstants**](ThorchainStringConstants.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

