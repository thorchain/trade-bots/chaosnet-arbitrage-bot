# NodeKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**secp256k1** | **str** | secp256k1 public key | [optional] 
**ed25519** | **str** | ed25519 public key | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

