# coding: utf-8

"""
    Midgard Public API

    The Midgard Public API queries THORChain and any chains linked via the Bifröst and prepares information about the network to be readily available for public users. The API parses transaction event data from THORChain and stores them in a time-series database to make time-dependent queries easy. Midgard does not hold critical information. To interact with BEPSwap and Asgardex, users should query THORChain directly.  # noqa: E501

    OpenAPI spec version: 1.0.0-oas3
    Contact: devs@thorchain.org
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class Tx(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'tx_id': 'str',
        'memo': 'str',
        'address': 'str',
        'coins': 'Coins'
    }

    attribute_map = {
        'tx_id': 'txID',
        'memo': 'memo',
        'address': 'address',
        'coins': 'coins'
    }

    def __init__(self, tx_id=None, memo=None, address=None, coins=None):  # noqa: E501
        """Tx - a model defined in Swagger"""  # noqa: E501
        self._tx_id = None
        self._memo = None
        self._address = None
        self._coins = None
        self.discriminator = None
        if tx_id is not None:
            self.tx_id = tx_id
        if memo is not None:
            self.memo = memo
        if address is not None:
            self.address = address
        if coins is not None:
            self.coins = coins

    @property
    def tx_id(self):
        """Gets the tx_id of this Tx.  # noqa: E501


        :return: The tx_id of this Tx.  # noqa: E501
        :rtype: str
        """
        return self._tx_id

    @tx_id.setter
    def tx_id(self, tx_id):
        """Sets the tx_id of this Tx.


        :param tx_id: The tx_id of this Tx.  # noqa: E501
        :type: str
        """

        self._tx_id = tx_id

    @property
    def memo(self):
        """Gets the memo of this Tx.  # noqa: E501


        :return: The memo of this Tx.  # noqa: E501
        :rtype: str
        """
        return self._memo

    @memo.setter
    def memo(self, memo):
        """Sets the memo of this Tx.


        :param memo: The memo of this Tx.  # noqa: E501
        :type: str
        """

        self._memo = memo

    @property
    def address(self):
        """Gets the address of this Tx.  # noqa: E501


        :return: The address of this Tx.  # noqa: E501
        :rtype: str
        """
        return self._address

    @address.setter
    def address(self, address):
        """Sets the address of this Tx.


        :param address: The address of this Tx.  # noqa: E501
        :type: str
        """

        self._address = address

    @property
    def coins(self):
        """Gets the coins of this Tx.  # noqa: E501


        :return: The coins of this Tx.  # noqa: E501
        :rtype: Coins
        """
        return self._coins

    @coins.setter
    def coins(self, coins):
        """Sets the coins of this Tx.


        :param coins: The coins of this Tx.  # noqa: E501
        :type: Coins
        """

        self._coins = coins

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Tx, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Tx):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
