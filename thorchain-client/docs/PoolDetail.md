# PoolDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**balance_rune** | **str** |  | [optional] 
**balance_asset** | **str** |  | [optional] 
**asset** | [**Asset**](Asset.md) |  | [optional] 
**pool_units** | **str** |  | [optional] 
**status** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

