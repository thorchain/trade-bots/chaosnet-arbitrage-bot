# Tx

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**Hash**](Hash.md) |  | [optional] 
**chain** | **str** |  | [optional] 
**from_address** | **str** |  | [optional] 
**to_address** | **str** |  | [optional] 
**coins** | [**Coins**](Coins.md) |  | [optional] 
**gas** | [**Gas**](Gas.md) |  | [optional] 
**memo** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

