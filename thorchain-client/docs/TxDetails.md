# TxDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tx** | [**Tx**](Tx.md) |  | [optional] 
**status** | **str** |  | [optional] 
**out_hashes** | [**list[Hash]**](Hash.md) |  | [optional] 
**block_height** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

