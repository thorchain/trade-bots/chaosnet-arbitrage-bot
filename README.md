sudo apt-get install build-essential libssl-dev libffi-dev python-dev zlib1g-dev libbz2-dev libsqlite3-dev wget curl llvm libncurses5-dev
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get install python3.5-dev python3.5-venv

wget https://www.python.org/ftp/python/3.5.6/Python-3.5.6.tgz
tar -xvzf Python-3.5.6.tgz
cd Python-3.5.6
make
sudo make install