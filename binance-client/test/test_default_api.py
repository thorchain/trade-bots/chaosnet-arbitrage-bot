# coding: utf-8

"""
    Midgard Public API

    The Midgard Public API queries THORChain and any chains linked via the Bifröst and prepares information about the network to be readily available for public users. The API parses transaction event data from THORChain and stores them in a time-series database to make time-dependent queries easy. Midgard does not hold critical information. To interact with BEPSwap and Asgardex, users should query THORChain directly.  # noqa: E501

    OpenAPI spec version: 1.0.0-oas3
    Contact: devs@thorchain.org
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.default_api import DefaultApi  # noqa: E501
from swagger_client.rest import ApiException


class TestDefaultApi(unittest.TestCase):
    """DefaultApi unit test stubs"""

    def setUp(self):
        self.api = DefaultApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_account(self):
        """Test case for get_account

        Get an account.  # noqa: E501
        """
        pass

    def test_get_closed_orders(self):
        """Test case for get_closed_orders

        Get closed orders.  # noqa: E501
        """
        pass

    def test_get_depth(self):
        """Test case for get_depth

        Get the order book.  # noqa: E501
        """
        pass

    def test_get_open_orders(self):
        """Test case for get_open_orders

        Get open orders.  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
