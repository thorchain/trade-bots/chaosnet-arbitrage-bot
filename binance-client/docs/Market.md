# Market

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**base_asset_symbol** | **str** |  | [optional] 
**quote_asset_symbol** | **str** |  | [optional] 
**list_price** | **str** |  | [optional] 
**tick_size** | **str** |  | [optional] 
**lot_size** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

