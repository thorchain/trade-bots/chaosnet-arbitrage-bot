# OrderDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cumulate_quantity** | **str** |  | [optional] 
**fee** | **str** |  | [optional] 
**last_executed_price** | **str** |  | [optional] 
**order_create_time** | **str** |  | [optional] 
**order_id** | **str** |  | [optional] 
**owner** | **str** |  | [optional] 
**price** | **str** |  | [optional] 
**quantity** | **str** |  | [optional] 
**side** | **int** |  | [optional] 
**status** | **str** |  | [optional] 
**symbol** | **str** |  | [optional] 
**transaction_hash** | **str** |  | [optional] 
**transaction_t_ime** | **str** |  | [optional] 
**type** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

