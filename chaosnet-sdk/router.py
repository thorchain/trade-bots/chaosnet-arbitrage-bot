# -*- coding: future_fstrings -*-
from logger import get_logger, logging
from midgard import MidgardApi
from binance import BinanceApi
from thorchain import ThorApi
from calculator import optimal
from decimal import Decimal, ROUND_DOWN
import time


router_logger = get_logger("router", level=logging.DEBUG)


class Router:
    def __init__(self,key,host=None):
        if host:
            self.midgard = MidgardApi(host=host)
        else:
            self.midgard = MidgardApi()
        self.binance = BinanceApi(key=key)
        self.thorNode = ThorApi(host='http://159.89.215.251:1317')
        self.selected = ['BNB.BNB', 'BNB.BUSD-BD1']
        self.binance.get_balance()

    def round_down(self, number, lot_size):
        return number.quantize(Decimal(lot_size), rounding=ROUND_DOWN)

    def binance_thor_rune_monitor(self, omega=Decimal('0.75'), max=Decimal('1000'), bnb_fee=0.000375, rune_fee=1):
        for pool in self.selected:
            info = self.thorNode.get_pool(pool=pool)
            asset = info.asset.split('.')
            chain = asset[0]
            symbol = asset[1]
            chain_address = self.thorNode.get_vault(chain=chain)
            asset_depth = Decimal(info.balance_asset) / 10**8
            rune_depth = Decimal(info.balance_rune) / 10**8
            rune_per_asset = rune_depth / asset_depth
            binance_depth = self.binance.depth(self.binance.RUNE, bnb=(symbol == self.binance.BNB))

            # route 1: sell Rune on Thor, buy Rune on Dex [gain in asset]
            # binance_ask_price = Decimal(binance_depth.asks[0][0])
            # binance_ask_volume = Decimal(binance_depth.asks[0][1])
            # fee_equivalent = rune_fee / rune_per_asset
            # thor_rune_in = self.round_down(binance_ask_volume * omega, '0.1')
            # if thor_rune_in > max:
            #     thor_rune_in = max
            # route1 = optimal(inputAmount=thor_rune_in, inputDepth=rune_depth, outputDepth=asset_depth, fee_output=fee_equivalent)
            # slices = route1[0]
            # thor_asset_out = route1[1] # - bnb fee
            # binance_asset_in = thor_rune_in * binance_ask_price
            # asset_gain = thor_asset_out - binance_asset_in
            # route1_gain = asset_gain * rune_per_asset
            # router_logger.info(f'route1 gain : {route1_gain}')
            # if route1_gain > 2:
            #     router_logger.info(
            #         f'asset depth [{asset_depth}] rune depth [{rune_depth}] rune per asset [{rune_per_asset}]')
            #     router_logger.info(
            #         f'ROUTE 1: THOR [{thor_rune_in} RUNE in {slices} Slices -> {thor_asset_out} {symbol}] '
            #         f'BINANCE [{thor_rune_in} RUNE <- {binance_asset_in} {symbol}] ')
            #     self.route1_execution(slices=slices, chain=chain, symbol=symbol, thor_rune_in=thor_rune_in,
            #                      binance_asset_in=binance_asset_in,chain_address=chain_address, binance_ask_price=binance_ask_price)
            # else:
            # route 2: sell rune on dex, buy rune on thor [gain in rune]
            binance_bid_price = Decimal(binance_depth.bids[0][0])
            binance_bid_volume = Decimal(binance_depth.bids[0][1])
            binance_rune_in = self.round_down(binance_bid_volume * omega, '0.1')
            if binance_rune_in > max:
                binance_rune_in = max
            binance_asset_out = binance_rune_in * binance_bid_price
            route2 = optimal(inputAmount=binance_asset_out, inputDepth=asset_depth, outputDepth=rune_depth, fee_output=rune_fee)
            slices = route2[0]
            thor_rune_out = route2[1]
            route2_gain = thor_rune_out - binance_rune_in
            router_logger.info(f'route2 gain : {route2_gain}')
            if route2_gain > 1.5:
                router_logger.info(
                    f'asset depth [{asset_depth}] rune depth [{rune_depth}] rune per asset [{rune_per_asset}]')
                router_logger.info(f'ROUTE 2: BINANCE [{binance_rune_in} RUNE -> {binance_asset_out} {symbol}] '
                                   f'THOR [{binance_asset_out} {symbol} in {slices} Slices -> {thor_rune_out} RUNE] ')
                # self.route2_execution(slices=slices, chain=chain, symbol=symbol, binance_rune_in=binance_rune_in,
                #                      binance_asset_out=binance_asset_out,chain_address=chain_address, binance_bid_price=binance_bid_price)


    def route1_execution(self, slices, chain, symbol, thor_rune_in, binance_asset_in, chain_address, binance_ask_price):
        if slices > 1:
            thor_hashes = self.binance.thor_smart_swap(chain=chain, i_symbol=self.binance.RUNE, o_symbol=symbol,
                                                       amount=thor_rune_in, to_address=chain_address,
                                                       limit=binance_asset_in, slice=slices)
            time.sleep(4)
            thor_confirmation = True
            for hash in thor_hashes:
                status = self.thorNode.get_tx_status(hash=hash)
                if status:
                    router_logger.warn(f'thor success [hash]: {hash}')
                else:
                    router_logger.error(f'thor failure [hash]: {hash}')
                    thor_confirmation = False
            if thor_confirmation:
                binance_response = self.binance.dex_buy(symbol=self.binance.RUNE, quantity=thor_rune_in,
                                                        price=binance_ask_price,
                                                        bnb=(symbol == self.binance.BNB))
                if binance_response.status == 'FullyFill':
                    router_logger.warning(f'binance success [hash]: {binance_response.transaction_hash}')
                else:
                    router_logger.error(f'binance fail [response]: {binance_response}')
                self.log_in_out_binance(binance_response)
        else:
            thor_hash = self.binance.thor_swap(chain=chain, i_symbol=self.binance.RUNE, o_symbol=symbol,
                                               amount=thor_rune_in, to_address=chain_address, limit=binance_asset_in)
            time.sleep(4)
            status = self.thorNode.get_tx_status(hash=thor_hash)
            if status:
                router_logger.warn(f'thor success [hash]: {thor_hash}')
                binance_response = self.binance.dex_buy(symbol=self.binance.RUNE, quantity=thor_rune_in,
                                                        price=binance_ask_price,
                                                        bnb=(symbol == self.binance.BNB))
                if binance_response.status == 'FullyFill':
                    router_logger.warning(f'binance success [hash]: {binance_response.transaction_hash}')
                else:
                    router_logger.error(f'binance fail [response]: {binance_response}')
                self.log_in_out_binance(binance_response)
            else:
                router_logger.error(f'thor failure [hash]: {thor_hash}')

    def route2_execution(self, slices, chain, symbol, binance_rune_in, binance_asset_out, chain_address, binance_bid_price):
        binance_response = self.binance.dex_sell(symbol=self.binance.RUNE, quantity=binance_rune_in,
                                                price=binance_bid_price,
                                                bnb=(symbol == self.binance.BNB))
        if binance_response.status == 'FullyFill':
            router_logger.warn(f'binance success [hash]: {binance_response.transaction_hash}')
            if slices > 1:
                thor_hashes = self.binance.thor_smart_swap(chain=chain, i_symbol=symbol, o_symbol=self.binance.RUNE,
                                                           amount=binance_asset_out, to_address=chain_address,
                                                           limit=binance_rune_in, slice=slices)
                time.sleep(4)
                for hash in thor_hashes:
                    router_logger.warn(f'executing {len(thor_hashes)} swaps')
                    status = self.thorNode.get_tx_status(hash=hash)
                    if status:
                        router_logger.warn(f'thor success [hash]: {hash}')
                    else:
                        router_logger.error(f'thor failure [hash]: {hash}')
            else:
                thor_hash = self.binance.thor_swap(chain=chain, i_symbol=symbol, o_symbol=self.binance.RUNE,
                                                           amount=binance_asset_out, to_address=chain_address,
                                                           limit=binance_rune_in)
                time.sleep(4)
                status = self.thorNode.get_tx_status(hash=thor_hash)
                # check wallet balance for failed attempt
                if status:
                    router_logger.warn(f'thor success [hash]: {thor_hash}')
                else:
                    router_logger.error(f'thor failure [hash]: {thor_hash}')
        else:
            router_logger.error(f'binance fail [response]: {binance_response}')
        self.log_in_out_binance(binance_response)

    def log_in_out_thor(self, thor_response):
        thor_in = thor_response._in.coins[0]
        thor_out = thor_response.out
        thor_out_str = " ".join(f'{int(out.coins[0]["amount"]) / 10 ** 8} {out.coins[0]["asset"]}' for out in thor_out)
        router_logger.info(f'ACTUAL: THOR [{int(thor_in["amount"]) / 10 ** 8} {thor_in["asset"]} -> {thor_out_str}] ')

    def log_in_out_binance(self, binance_response):
        b_quantiy = float(binance_response.quantity)
        b_price = float(binance_response.price)
        if binance_response.side == 1:
            router_logger.info(f'ACTUAL: BINANCE {b_quantiy} <- {b_price * b_quantiy} [{binance_response.symbol}]')
        else:
            router_logger.info(f'ACTUAL: BINANCE {b_quantiy} -> {b_price * b_quantiy} [{binance_response.symbol}]')


